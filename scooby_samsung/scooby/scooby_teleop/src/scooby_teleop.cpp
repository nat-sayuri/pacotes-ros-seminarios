// Copyright 2020 Giovani Bernardes.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include "scooby_teleop.h"
#include "ui_scooby_teleop.h"

#include <ctime>
#include <QDebug>
#include "joypad.h"


scooby_teleop::scooby_teleop(rclcpp::Node::SharedPtr& nh, QWidget *parent) :
    QMainWindow(parent),
    nh_(nh),
    ui(new Ui::scooby_teleop)
{
    ui->setupUi(this);

    connect(ui->widget, &JoyPad::xChanged, this, [this](float x){
        idy = -x;
    	SendCommand();
    });


    connect(ui->widget, &JoyPad::yChanged, this, [this](float y){
        idx = -y;
        SendCommand();
    });


   srand(time(NULL));

   update_timer_ = new QTimer(this);
   update_timer_->setInterval(20);
   update_timer_->start();

   connect(update_timer_, SIGNAL(timeout()), this, SLOT(onUpdate()));
 
    auto default_qos = rclcpp::QoS(rclcpp::SystemDefaultsQoS());

    // Advertise velocity commands
    cmd_pub_ = nh_->create_publisher<geometry_msgs::msg::Twist>("scooby/cmd_vel", default_qos);
 
    //laser_sub_ = nh_->create_subscription<sensor_msgs::msg::LaserScan>(
    //  "scan", default_qos,
    //  std::bind(&scooby_teleop::OnSensorMsg, this, std::placeholders::_1));


}

scooby_teleop::~scooby_teleop()
{
    delete ui;
}


void scooby_teleop::SendCommand()
  {
  
    int scale;// = ui->horizontalSlider->value();
    RCLCPP_INFO(nh_->get_logger(), "Linear Velocity [%f] and Angular Velocity [%f] scale: [%f] ", idx, idy, scale);


    auto cmd_msg = std::make_unique<geometry_msgs::msg::Twist>();
    cmd_msg->linear.x = idx;
    cmd_msg->angular.z = idy;
      
    cmd_pub_->publish(std::move(cmd_msg));
  }
  
  
void scooby_teleop::onUpdate()
{

  //RCLCPP_INFO(nh_->get_logger(), "OnUpdate ...");

  if (!rclcpp::ok())
  {
    close();
    return;
  }

   //rclcpp::spin_some(nh_);
   
}


  void scooby_teleop::OnSensorMsg(const sensor_msgs::msg::LaserScan::SharedPtr _msg)
  {
       RCLCPP_INFO(nh_->get_logger(), "OnSensorMsg Activated.. DATA_SIZE [%d] ", _msg->ranges.size());

  }
