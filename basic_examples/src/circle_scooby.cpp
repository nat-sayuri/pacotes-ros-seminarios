#include <chrono>
#include <functional>
#include <memory>
#include <string>
#include <utility>

#include "rclcpp/rclcpp.hpp"
//#include "std_msgs/msg/string.hpp"
#include <geometry_msgs/msg/twist.hpp>

#include <sensor_msgs/msg/laser_scan.hpp>
using std::placeholders::_1;
using namespace std::chrono_literals;

/* 
A class faz uma publicacao do topico 
para modificar o parametro fazer
ros2 param set /sooby_circle vel_scooby 2.0


subscripcao ao ros2 topic echo /scooby/sensors/lasers/rear_left_data
 */

class MinimalPublisher : public rclcpp::Node
{
  public:
    MinimalPublisher()
    : Node("sooby_circle"), count_(0)
    {
      auto default_qos = rclcpp::QoS(rclcpp::SystemDefaultsQoS());
      publisher_ = this->create_publisher<geometry_msgs::msg::Twist>("scooby/cmd_vel", 10);

      /// 3o Exercício: Subscriber para os dados do laser do robô
      subscription_ = this->create_subscription<sensor_msgs::msg::LaserScan>(
        "/scooby/sensors/laser_scan",  default_qos,std::bind(&MinimalPublisher::read_laser, this, _1));
      
      // subscription_ = this->create_subscription<std_msgs::msg::String>(
      // "topic", 10, std::bind(&MinimalSubscriber::topic_callback, this, _1));

      this->declare_parameter<double>("vel_scooby", 1.0);
      timer_ = this->create_wall_timer(500ms, std::bind(&MinimalPublisher::timer_callback, this));
    }

  private:
  
  /// 3o Exercício: Subscriber para os dados do laser do robô
  void read_laser(const sensor_msgs::msg::LaserScan::SharedPtr _msg){
    bool stop = false;
    for (size_t i = 0; i < _msg->ranges.size(); i++)
    {
      auto medicao = _msg->ranges[i];
      if(medicao<20){
        RCLCPP_INFO(this->get_logger(),"Medicao: %f",medicao);
        stop = true;
      }
    }
    auto message = geometry_msgs::msg::Twist();
    if (stop)
    {
      count_=1000;
      message.linear.x = 0.0;
      message.angular.z = 0.0;
    }else
    {
      count_=0;
      message.linear.x = 0.5;
      message.angular.z = 0.5;
    }
    publisher_->publish(message); 
  }

  void timer_callback()
  {
    auto message = geometry_msgs::msg::Twist();
    //// 1o Exercício: Mover o robô em cículos 
    // message.linear.x = 0.5;
    // message.angular.z = 0.5;
    // RCLCPP_INFO(this->get_logger(), "linear vel: '%.2f, angular vel: %.2f", 
    //   message.linear.x,message.angular.z);
    
    //// 2o Exercício: Mover o robô em cículos até o contador chegar a um determinado valor 
    double parameter_vel_;
    this->get_parameter("vel_scooby", parameter_vel_);
    
    if(count_<200){
      message.linear.x = 0.5*parameter_vel_;
      message.angular.z = 0.5*parameter_vel_;
    }else{
      message.linear.x = 0.0;
      message.angular.z = 0.0;
    }
    RCLCPP_INFO(this->get_logger(), "linear vel: '%.2f, angular vel: %.2f, --- %f ---%d", 
      message.linear.x,message.angular.z,parameter_vel_,count_);
    publisher_->publish(message);
    count_++;
  }
  
  rclcpp::TimerBase::SharedPtr timer_;
  rclcpp::Publisher<geometry_msgs::msg::Twist>::SharedPtr publisher_;
  rclcpp::Subscription<sensor_msgs::msg::LaserScan>::SharedPtr subscription_;
  size_t count_;
};

  int main(int argc, char * argv[])
  {
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<MinimalPublisher>());
    rclcpp::shutdown();
    return 0;
  }