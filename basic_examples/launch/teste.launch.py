from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([
        Node(
            package='turtlesim',
            executable='turtlesim_node',
            name='sim',
            remappings=[
                ('turtle1/cmd_vel', 'scooby/cmd_vel')
            ]
        ),
        Node(
            package='basic_examples',
            #namespace='scooby',
            executable='circle_scooby',
            name='circle',
            parameters=[{'vel_scooby': 2.0 }]
        )    ])



        